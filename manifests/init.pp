# == Class: profile_firewall
#
# Full description of class profile_firewall here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { profile_firewall:
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile_firewall {

  $firewall_rules = hiera('profile_firewall::rules')

  validate_hash($firewall_rules)

  # Make sure pre is loaded before post
  Firewall {
    before  => Class['profile_firewall::post'],
    require => Class['profile_firewall::pre'],
  }

  # include pre and post classes
  class { ['profile_firewall::pre', 'profile_firewall::post']: }

  # load the firewall class to ensure installation
  class { 'firewall': }

  # load firewall rules
  create_resources('firewall', $firewall_rules)
}
