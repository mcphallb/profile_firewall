class profile_firewall::pre {

  Firewall {
    require => undef,
  }

  # Purge ipv4 rules
  resources {"firewall":
    purge => true
  }

  # Default rules
  firewall { '000 accept all icmp':
    proto  => 'icmp',
    action => 'accept',
  }->
  firewall { '001 accept related established rules':
    proto  => 'all',
    state  => ['RELATED', 'ESTABLISHED'],
    action => 'accept',
  }
}
